// ==UserScript==
// @name         NetflixSkip
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Skip intro, recap and annonces
// @author       cristalcorp
// @match        https://www.netflix.com/watch/*
// @grant        none
// ==/UserScript==


(function() {
    'use strict';

    var ready = true;
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    var intro_skipper = setInterval(function(){
        if(document.getElementsByClassName('button-primary watch-video--skip-content-button medium hasLabel ltr-ublg01')[0] && ready){
            document.getElementsByClassName('button-primary watch-video--skip-content-button medium hasLabel ltr-ublg01')[0].click();
            }
        }, 500);
    var next_episode = setInterval(function(){
        if(document.getElementsByClassName('color-primary hasLabel hasIcon ltr-v8pdkb')[0] && ready){
            sleep(5000);
            document.getElementsByClassName('color-primary hasLabel hasIcon ltr-v8pdkb')[0].click();
            }
        }, 500);
})();

